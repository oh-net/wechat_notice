<?php

/**
 * 使用微信企业号作为警报源的推送功能
 * User: regchen
 * Date: 17-1-5
 * Time: 下午1:39
 */
class WechatNotice
{
    private $token_file_path;
	private $corpId;
	private $corpSecret;
	private $accessToken;
	private $agentId;
	private $encodingAesKey;
	private $default_media_id;
	private $sys_media;
	private $CI;

	public function __construct()
	{
		$this->CI = get_instance();
		$this->token_file_path = $this->CI->config->item('wechat_tmp_token_save_path');
		$this->corpId = $this->CI->config->item('wechat_corp_id');
		$this->corpSecret = $this->CI->config->item('wechat_corp_secret');
		$this->agentId = $this->CI->config->item('wechat_agent_id');
		$this->encodingAesKey = $this->CI->config->item('wechat_encoding_AES_key');

		$this->sys_media = $this->CI->config->item('wechat_media');
		$this->default_media_id = $this->sys_media['image']['notice'];
		$this->accessToken = $this->get_token();
	}

	/**
	 * 发送消息，参数类型请参见内部调用
	 * @return mixed
	 */
	public function publish()
	{
		$data_response=[];
		if (empty($this->accessToken)) {
            $this->accessToken = $this->get_token();
		}

		if (!empty($this->accessToken)){
            $arr_param = func_get_args();

            print_r($arr_param);
            $arr_post = call_user_func_array(array($this, 'generate_data'), $arr_param);

            $url           = 'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=' . $this->accessToken;
            $data_response = $this->post_request_with_curl($url, $arr_post);
        }

		return $data_response;
	}

	/**
	 * @param string $msg_content 消息内容
	 * @param string $msg_title 消息标题，未指定时，使用 $msg_content
	 * @param string $to_user 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
	 * @param string $to_party 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
	 * @param string $to_tag 标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
	 * @param string $media_id 实际的 media_id 或者 媒体文件Name, 将根据config中的Name获取实际的 media_id
	 * @param string $msg_type 消息类型（可选项：text,image,voice,video,file,news,mpnews）
	 * @param string $description 简介
	 * @param string $url 新闻链接，或点击“阅读原文”的链接
	 * @param string $pic_url 图文消息的 图片链接
	 * @param string $safe 是否保密
	 *
	 * @return array
	 */
	private function generate_data($msg_content,$msg_title='',$to_user='',$to_party='',$to_tag='',$media_id='',$msg_type='mpnews',$description='',$url='',$pic_url='',$safe='0')
	{
		$_title = empty($msg_title) ? $msg_content : $msg_title;

		if (empty($media_id)){
			$media_id = $this->default_media_id;
		}else{
			// 如果config中的默认media中有对应的 media_id 关键字，则使用默认关键字所对应的media_id
			if (in_array($media_id,array_keys($this->sys_media['image']))){
				$media_id = $this->sys_media['image'][$media_id];
			}
		}
		if (empty($description)) {
			$description = mb_strcut(strip_tags($msg_content), 0, 100) . '...' . date('[m-d H:i]');;
		}

		$arr_post = array(
			'touser' => "$to_user",
			'toparty' => "$to_party",
			'msgtype' => "$msg_type",
			'totag' => "$to_tag",
			'agentid' => $this->agentId,
			'safe' => $safe
		);

		switch ($msg_type) {
			case 'text':
				$_content = mb_strcut($msg_content, 0, 2048);
				$arr_post['text']['content'] = "$_content";
				break;
			case 'image':
				$arr_post['image']['media_id'] = "$media_id";
				break;
			case 'voice':
				$arr_post['voice']['media_id'] = "$media_id";
				break;
			case 'video':
				$arr_post['video'] = array(
					'media_id' => "$media_id",
					'title' => "$_title",
					'description' => "$description",
				);
				break;
			case 'file':
				$arr_post['file']['media_id'] = "$media_id";
				break;
			case 'news':
				$arr_post['news']['articles'] = array(
					array(
						'title' => "$_title",
						'description' => "$description",
						'url' => "$url",
						'picurl' => "$pic_url",
					)
				);
				break;
			case 'mpnews':
				$_content = mb_strcut($msg_content, 0, 666000,"UTF-8");
				$arr_post['mpnews']['articles'] = array(
					array(
						"title" => "$_title",
						"thumb_media_id" => "$media_id",
						"author" => "EventLogger",
						"content_source_url" => "$url",
						"content" => "$_content",
						"digest" => "$description",
						"show_cover_pic" => "1"
					)
				);
				break;
		}

		return $arr_post;
	}

	/**
	 * 保存最新的Token到目录下的文件中
	 * @param string $token
	 * @param string $expire_time
     * @return bool
	 */
	private function save_token($token, $expire_time)
	{
	    try {
            $flag = file_put_contents($this->token_file_path . 'token.txt', $token . '|' . $expire_time);
            if ($flag === false){
                die("put token to token.txt failed.");
            }
        }catch (Exception $exception){
	        die("save token.txt to current folder failed!");
        }

        return $flag;
	}

	/**
	 * 获取当前存在的Token
	 * @return array
	 */
	private function get_token()
	{
		$_token=null;
		if (true === file_exists($this->token_file_path . 'token.txt')) {
			$_token    = file_get_contents($this->token_file_path . 'token.txt');
			$arr_token = explode('|', $_token);
		}

		if (empty($arr_token) || !isset($arr_token[1]) || (int)$arr_token[1] < time()) {
			// 保存在or已过期，重新请求
			$url = sprintf('https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s',
				$this->corpId,
				$this->corpSecret
			);
			$req_result = file_get_contents($url);
			$arr_token = json_decode($req_result,true);

			if (!empty($arr_token) && isset($arr_token['access_token']) && isset($arr_token['expires_in'])) {
			    $arr_result = json_decode($req_result, true);
				$_token = $arr_result['access_token'];
				$expire = $arr_result['expires_in'];
				// save token
				$this->save_token($_token, $expire);
			}
		}

		return $_token;
	}

	/**
	 * 通过 curl post 数据，并返回结果
	 * @param string $url
	 * @param array $arr_data
	 *
	 * @return mixed
	 */
	private function post_request_with_curl($url,$arr_data)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arr_data,JSON_UNESCAPED_UNICODE));
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	}
}