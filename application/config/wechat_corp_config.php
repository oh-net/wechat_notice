<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * 微信企业号配置文件
 * User: regchen
 * Date: 17-1-5
 * Time: 下午1:40
 */

$config['wechat_encoding_AES_key'] = '';
$config['wechat_token'] = '';
$config['wechat_corp_id'] = '';

$config['wechat_corp_secret'] = '';
$config['wechat_agent_id'] = 1000002; // 应用ID
$config['wechat_tmp_token_save_path'] = '/home/public/'; // 需要有斜线结尾
//wechat系统资源，可以在程序中直接使用
$config['wechat_media'] = array(
	'image'=>array(
		'notice'=>'2-ikXpMc78A8t6GiXX3KVYb2wZwoygr6E8B78qfUPAplRIg93KWTyMz5GdTn6CQ84',
		'info'=>'2Mgu87FtMhQkTWmonXbOTm2iQ0chnGfG7BJtaDLRjQnPpB65kLAXpN_xbzNV6D7sO',
		'alert'=>'2bKY8rZhAVoGqbVFF_CvsMYSfyjIhYON7nt3XNMFEz1YTRjbdZHZdR00IuOOTc7Hj',
		'warn'=>'2bKY8rZhAVoGqbVFF_CvsMYSfyjIhYON7nt3XNMFEz1YTRjbdZHZdR00IuOOTc7Hj',
		'error'=>'2dFh-0E3amI_XLPI86IMegDLgr5Re_1g_E1sTuVbvSxGL8GL40smmcDQunEGKAaJs',
	),
	'file'=>array(

	),
	'video'=>array(

	),
	'voice'=>array(

	)
);
