<?php

/**
 * Created by PhpStorm.
 * User: regchen
 * Date: 17-1-6
 * Time: 下午3:02
 */
class Notice extends CI_Controller
{
	public function index()
	{
		$msg_content = isset($_GET['msg_content']) ? $_GET['msg_content'] : '';
		$msg_title   = isset($_GET['msg_title']) ? $_GET['msg_title'] : 'Default Title'.'['.date('H:i:s').']';
		$to_user     = isset($_GET['to_user']) ? $_GET['to_user'] : '';
		$to_party    = isset($_GET['to_party']) ? $_GET['to_party'] : '';
		$to_tag      = isset($_GET['to_tag']) ? $_GET['to_tag'] : '';
		$media_id    = isset($_GET['media_id']) ? $_GET['media_id'] : '';
		$msg_type    = isset($_GET['msg_type']) ? $_GET['msg_type'] : 'text';
		$description = isset($_GET['description']) ? $_GET['description'] : '';
		$url         = isset($_GET['url']) ? $_GET['url'] : '';
		$pic_url     = isset($_GET['pic_url']) ? $_GET['pic_url'] : '';
		$safe        = isset($_GET['safe']) ? $_GET['safe'] : '';

		// 消息 content 可能会有比较多的数据,所以该内容允许使用 POST 方式传值
        if (empty($msg_content)){
            $msg_content = isset($_POST['msg_content']) ? $_POST['msg_content'] : '';
        }

		if (!empty($msg_content)
			&& (!empty($to_tag) || !empty($to_party) || !empty($to_user))
		) {
			$wechat_notice = new WechatNotice();
			print_r($wechat_notice->publish($msg_content, $msg_title, $to_user, $to_party, $to_tag, $media_id, $msg_type, $description, $url, $pic_url, $safe));
		} else {
			echo "wechat_notice: Parameters invalid\n";
		}
	}
}