### Requirement
* PHP 5.4+
* PHP-model: php-curl (or php5-curl, php7-curl etc,)

### Parameters List:
* **`msg_content`** -- **必选项** 消息内容,支持 `GET` 和 `POST` 两种方式发送
* **`msg_title`** -- 消息标题, 如不指定, 将从`msg_content` 中截取
* **`to_user`** -- 送达的用户名,如:`reg.chen,john.zhang`
* **`to_party`** -- 送达的部门id 
* **`to_tag`** --  送达的标签id
* **`media_id`** -- 媒体编号, 内置选项: `info`,`notice`,`alert`,`warn`,`error`
* **`msg_type`** -- 消息类型 可选项`text`,`file`,`image`,`voice`,`video`,`news`,`mpnews`
* **`description`** -- `news` 和 `mpnews` 类型消息的简介
* **`url`** -- `news` 和 `mpnews` 类型消息的跳转url
* **`pic_url`** -- 封面图片的url
* **`safe`** -- 是否尽收件人可见(1 or 0)

### Calling Demo(using GET):
` localhost/wechat_notice/index.php?msg_content=hello123123123msg_title=TitleDEMO&to_user=reg.chen`

### config file:
 >  **`application/config/wechat_corp_config.php`**
 
**Attention**:  
`$config['wechat_tmp_token_save_path']` 指定的路径必须要有可写权限(writable) 
